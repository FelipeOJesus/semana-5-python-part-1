def fizzbuzz (x):
    if(x % 3 == 0 and x % 5 == 0):
        return "FizzBuzz"
    if(x % 3 == 0):
        return "Fizz"
    if(x % 5 == 0):
        return "Buzz"
    return x

def main():
    print (fizzbuzz(3))
    print (fizzbuzz(5))
    print (fizzbuzz(15))
    print (fizzbuzz(4))

main()
