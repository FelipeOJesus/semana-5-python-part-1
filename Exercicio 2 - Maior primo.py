def maior_primo (x):
    i = 1
    if(x >=2):
        while(not ePrimo(x) and i <= x):
            if(not ePrimo(x)):
                i = 1
                x -=1
            i+=1
        return x

def ePrimo(k):
    condicaoPrimo = False
    i = 1
    divisaoSemResto = 0

    while(i<=k):
        if(k % i == 0):
            divisaoSemResto += 1
        i+=1

    if(divisaoSemResto == 2):
        condicaoPrimo = True
        return condicaoPrimo

    return condicaoPrimo


def main():
    print (maior_primo(100))
    print (maior_primo (7))

main()
